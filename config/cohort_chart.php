<?php

return [

    /*
    |--------------------------------------------------------------------------
    | USER REGISTRATION STEPS
    |--------------------------------------------------------------------------
    |
    | Number of steps of user registration process
    |
    */

    'user_steps_count' => 7,
];
