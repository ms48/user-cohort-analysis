<!-- Nav Item - Dashboard -->
<li class="nav-item {{ (request()->is('cohort-analysis')) ? 'active' : '' }}" >
  <a class="nav-link" href="{{url('cohort-analysis')}}">
    <i class="fas fa-fw fa-chart-pie"></i>
    <span>Cohort Analysis</span></a>
</li>