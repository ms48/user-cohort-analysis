@extends('layouts.main')
@section('title', 'Cohort Chart')
@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">
            <div class="row">
                <div class="col-6">
                    Cohort Chart
                </div>
            </div>
        </h5>
    </div>
    <div class="card-body"> 
        <cohort-chart></cohort-chart>
    </div>
</div>
@endsection