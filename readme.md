# User Cohort Analysis
A demo laravel app which populate weekly cohort analysis chart by using CSV data source.

# System Requirements
* PHP >= 7.1.3
    * BCMath PHP Extension
    * Ctype PHP Extension
    * JSON PHP Extension
    * Mbstring PHP Extension
    * OpenSSL PHP Extension
    * PDO PHP Extension
    * Tokenizer PHP Extension
    * XML PHP Extension
    * Mod Rewrite PHP Extenstion
* MySql >= 5.7
* NodeJs (For compiling the assets)
* NPM
* [Composer](https://getcomposer.org/)
### Configuration/Installation

Follow below mentioned instructions after cloning the repository

**Install composer packages**

```sh
$ composer install
```
**Database**

1. Go to the repository folder
2. Copy the .env.example to .env
3. Open .env file and change following database settings according to your database configuration.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=user-cohort-analysis-db
DB_USERNAME=root
DB_PASSWORD=
```
Migrate and seed the database,
```sh
$ php artisan migrate --seed
```
**NodeJs**

To compile/ Install assets, run following from the root folder of the project

```sh
$ npm install
$ npm run dev
```
### Run (Local Development Server)

Since this was written on [Laravel Framework](https://laravel.com/docs/5.8/installation), you may use the serve Artisan command. This command will start a development server at http://localhost:8000:
```sh
$ php artisan serve
```
### Test

Run following from the root folder of the project.

From windows,
```
 .\vendor\bin\phpunit
```
From Linux,
```sh
 $ vendor/bin/phpunit
```

### External Plugins

| Plugin | README |
| ------ | ------ |
| Highchart | https://www.highcharts.com/ |
| Jquery | https://jquery.com/
| Bootstrap | https://getbootstrap.com/ |
| SB Admin 2 Bootstrap Theme | https://startbootstrap.com/themes/sb-admin-2/ 

### Screenshot
![Screenshot](screens/screenshot1.png)

### License
MIT