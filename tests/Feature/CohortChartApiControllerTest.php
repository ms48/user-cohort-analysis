<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CohortChartApiControllerTest extends TestCase
{
    use DatabaseMigrations;
    
    public function testChartDataGettingValidData()
    {
        $this->seed(\DatabaseSeeder::class);
        
        $response = $this->get('api/cohort-analysis/data');
        
        //assert response
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
        
        //assert data
        $chartData = json_decode($response->getContent(), true);
        $this->assertIsArray($chartData);
        $this->assertIsArray($chartData['data']);
        
        //check array inside data highchart
        $data = reset($chartData['data']);
        $this->assertNotEmpty($data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('data', $data);
    }
}
