<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CohortChartControllerTest extends TestCase
{
    use DatabaseMigrations;
        
    public function testChartViewLoading()
    {
        $this->seed(\DatabaseSeeder::class);
        
        $response = $this->get('cohort-analysis');
        
        //assert response
        $response->assertStatus(200)
            ->assertViewIs('cohort_chart')
            ->assertSee('Cohort Chart');
    }
}
