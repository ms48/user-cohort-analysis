<?php

namespace Tests\Unit;

use Tests\TestCase;
use Mockery;
use App\Services\CohortChartService;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CohortChartServiceTest extends TestCase
{
    use DatabaseMigrations;    
    protected $data;
    
    public function setUp() :void 
    { 
        parent::setUp();
        $this->data = [
            [
                "week_start" => "2016-07-18",
                "week_name" => "2016/29",
                "total_count" => "47",
                "step1" => "47",
                "step2" => "47",
                "step3" => "19",
                "step4" => "17",
                "step5" => "17",
                "step6" => "17",
                "step7" => "13"
            ],
            [
                "week_start" => "2016-07-25",
                "week_name" => "2016/30",
                "total_count" => "176",
                "step1" => "175",
                "step2" => "175",
                "step3" => "89",
                "step4" => "83",
                "step5" => "75",
                "step6" => "75",
                "step7" => "43"
            ], [
                "week_start" => "2016-08-01",
                "week_name" => "2016/31",
                "total_count" => "67",
                "step1" => "67",
                "step2" => "67",
                "step3" => "41",
                "step4" => "40",
                "step5" => "35",
                "step6" => "35",
                "step7" => "13"
            ]
        ];
    }

    public function testMakeChartReturningValidData()
    {
        $this->seed(\DatabaseSeeder::class);
        
        //mock dependancies
        $mock = Mockery::mock('App\Repositories\Interfaces\UserOnboardingRepositoryInterface');
        $service = (new CohortChartService($mock));
        $chartData = $service->makeChartArray($this->data);
        $this->assertIsArray($chartData);
        $this->assertNotEmpty($chartData);
        
        //validate data
        $data = reset($chartData);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertNotEmpty($data['data']);
        $this->assertNotEmpty($data['name']);
    }
}
