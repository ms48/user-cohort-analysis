<?php

use JeroenZwart\CsvSeeder\CsvSeeder;

class UserOnboardingsTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        /*
         * CsvSeeder configs
         */
        $this->file = '/database/seeds/csvs/export.csv';
        $this->tablename = 'user_onboardings';
        $this->truncate = true;
        $this->timestamps = false;
        $this->delimiter = ';';
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
	    DB::disableQueryLog();
	    parent::run();
    }
}
