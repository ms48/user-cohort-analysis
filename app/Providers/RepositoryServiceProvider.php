<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\UserOnboardingRepositoryInterface;
use App\Repositories\DbUserOnboardingRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //bind user onboarding repo to interface
        $this->app->bind(
            UserOnboardingRepositoryInterface::class, 
            DbUserOnboardingRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
