<?php

namespace App\Repositories\Interfaces;

interface UserOnboardingRepositoryInterface
{
    /**
     * Get weekly user onboarding stats by steps
     * 
     * @return array
     */
    public function getStats();
}
