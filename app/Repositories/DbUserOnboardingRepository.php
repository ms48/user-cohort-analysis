<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserOnboardingRepositoryInterface;
use App\Models\UserOnboarding;
use DB;

class DbUserOnboardingRepository implements UserOnboardingRepositoryInterface
{
    
    public function getStats(): array
    {
        $res = UserOnboarding::select(
                DB::raw('DATE_ADD(created_at, INTERVAL(2-DAYOFWEEK(created_at)) DAY) AS week_start'),
                DB::raw('CONCAT(YEAR(created_at), "/", WEEK(created_at)) AS week_name'),
                DB::raw('SUM(CASE WHEN onboarding_percentage <= 100 THEN 1 ELSE 0 END) AS total_count'),
                DB::raw('SUM(CASE WHEN onboarding_percentage > 0 AND onboarding_percentage <= 100 THEN 1 ELSE 0 END) step1'),
                DB::raw('SUM(CASE WHEN onboarding_percentage > 20 AND onboarding_percentage <= 100 THEN 1 ELSE 0 END) step2'),
                DB::raw('SUM(CASE WHEN onboarding_percentage > 40 AND onboarding_percentage <= 100 THEN 1 ELSE 0 END) step3'),
                DB::raw('SUM(CASE WHEN onboarding_percentage > 50 AND onboarding_percentage <= 100 THEN 1 ELSE 0 END) step4'),
                DB::raw('SUM(CASE WHEN onboarding_percentage > 70 AND onboarding_percentage <= 100 THEN 1 ELSE 0 END) step5'),
                DB::raw('SUM(CASE WHEN onboarding_percentage > 90 AND onboarding_percentage <= 100 THEN 1 ELSE 0 END) step6'),
                DB::raw('SUM(CASE WHEN onboarding_percentage = 100 THEN 1 ELSE 0 END) step7')
            )
            ->groupBy('week_name')
            ->groupBy('week_start')
            ->orderBy(DB::raw('YEAR(created_at)'),'ASC')
            ->orderBy(DB::raw('WEEK(created_at)'),'ASC')
            ->get();
        //return elequent result as array
        return (!$res) ? [] : $res->toArray();
    }
}
