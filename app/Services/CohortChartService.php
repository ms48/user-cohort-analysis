<?php

namespace App\Services;

Use App\Repositories\Interfaces\UserOnboardingRepositoryInterface;

class CohortChartService
{
    protected  $repo;
    
    public function __construct(UserOnboardingRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }
    
    /**
     * Get data from data source and process for chart
     * 
     * @return array
     */
    public function makeChart()
    {
        $statsData = $this->repo->getStats();
        return $this->makeChartArray($statsData);
    }
    
    /**
     * Make chart series array from user onboarding stats
     * 
     * @param array $statsData
     * @return array
     */
    public function makeChartArray($statsData)
    {
        $data = [];
        foreach ($statsData as $week){
            $steps = [];

            for($i = 0; $i <= config('cohort_chart.user_steps_count'); $i++){
                if($i == 0){
                    $steps[] = 100;
                }else{                    
                    $currentStep = "step".$i;
                    $steps[] = round(($week[$currentStep]/$week['total_count']) * 100);
                }
            }

            $data[] = [
                "name" => $week['week_start'],
                "data" => $steps
            ];
        }
        
        return $data;
    }    
}
