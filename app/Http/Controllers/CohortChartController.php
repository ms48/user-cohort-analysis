<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Controllers\Controller;
use App\Services\CohortChartService;

class CohortChartController extends Controller
{
    protected $chartService;
    
    public function __construct(CohortChartService $chartService)
    {
        $this->chartService = $chartService;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cohort_chart');
    }
    
    /**
     * Get Chart Data
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        $data = $this->chartService->makeChart();
        return Response::json([
            'success' => true,
            'data' => $data
        ], 200); 
    }
}
