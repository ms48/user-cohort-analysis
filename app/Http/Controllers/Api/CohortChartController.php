<?php

namespace App\Http\Controllers\Api;

use Response;
use App\Http\Controllers\Api\ApiController;
use App\Services\CohortChartService;

class CohortChartController extends ApiController
{
    protected $chartService;
    
    public function __construct(CohortChartService $chartService)
    {
        $this->chartService = $chartService;
    }
    
    /**
     * Get Chart Data
     *
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        $data = $this->chartService->makeChart();
        return $this->respond($data); 
    }
}
