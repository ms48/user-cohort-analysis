<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Response;

class ApiController extends Controller
{

    protected $status_code = 200;

    /**
     * Get response status code
     * 
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * Set Status code
     * 
     * @param int $status_code
     */
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;
        return $this;
    }

    /**
     * Set common API respond
     * 
     * @param array $data
     * @param array $headers
     * @return json
     */
    public function respond($data, $headers = [])
    {
        return Response::json(
            array_merge(['success' => true], ['data' => $data]), 
            $this->getStatusCode(), 
            $headers
        );
    }
}
